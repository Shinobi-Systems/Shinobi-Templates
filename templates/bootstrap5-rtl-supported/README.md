# bootstrap5-rtl-supported Template

- Original Template Demo : https://getbootstrap.com/docs/5.0/examples/dashboard/
- Listed on : https://getbootstrap.com/docs/5.0/examples/

1. You must copy the folder named `bootstrap5-rtl-supported` to your Shinobi directory in the `web/templates` folder. If you do not have a templates folder then you must create one.

2. Create the `renderPaths` object inside `conf.json`. If it already exists then remove the old one and place this one.
```
"renderPaths": {
  "home" : "templates/bootstrap5-rtl-supported/views/index",
  "handler" : "templates/bootstrap5-rtl-supported/handler"
}
```
3. Your `conf.json` file may look like this afterward.

```
{
  "port": 8080,
...
  "renderPaths": {
      "home" : "templates/bootstrap5-rtl-supported/views/index",
      "handler" : "templates/bootstrap5-rtl-supported/handler"
  }
}
```
4. Restart Shinobi
```
pm2 restart camera
```

# Important differences from Dashboard V2

- Tab system, you can go backward on the sequence of Pages that were accessed.
- Redesigned Front-End Framework with Bootstrap 5, No longer any "Material Design" components.
- Seeing events in the video list.
    - Shows Object tag and how many frames had objects as well as how many frames each object appeared for.
- Global Log stream is now in the Log Viewer
- Region Editor and Event Filters are no longer dependent on the Monitor Settings page
- Better display of sub categories within a page inside the Side Menu
- The original home screen is now a separate page called Live Grid
    - The Live Grid will open automatically if at least monitor was open before the page was reopened.
- Active Users are now displayed in the Sub-Account Manager (may be renamed to Account Manager)
- `addStorage` indicators moved to under the Main Storage indicator.
